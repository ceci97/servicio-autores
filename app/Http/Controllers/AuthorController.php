<?php

namespace App\Http\Controllers;


use App\Author;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;

class AuthorController extends Controller
{
    use ApiResponser;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**return authors list
     * 
     * @return Illuminate\Http\Response
     */

    public function index(){
        $authors = Author::all();
        return $this->successResponse($authors);
    }


    public function store(Request $request) {
        $rules = [
            'name'=> 'required|max:100',
            'gender'=> 'required|max:6|in:male,female',
            'country'=> 'required|max:80'
        ];

        $this->validate($request, $rules);

        $author = Author::create($request->all());

        return $this->successResponse($author, Response::HTTP_CREATED);
    }

    public function update(Request $request, $author)
    {
        $rules = [
            'name'=> 'required|max:100',
            'gender'=> 'required|max:6|in:male,female',
            'country'=> 'required|max:80'
        ];

        $this->validate($request, $rules);
        $author = Author::findOrFail($author);
        $author->fill($request->all());

        if($author->isClean()){
            return $this->errorResponse(
                'Al menos un valor debe ser cambiado',
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $author->save();
        return $this->successResponse($author);
    }


    public function show($author){
        $author = Author::findOrFail($author);
        return $this->successResponse($author);

    }


    public function destroy($author){
        $author = Author::findOrFail($author);
        $author->delete();
        return $this->successResponse($author);
    }


}
